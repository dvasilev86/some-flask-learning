from flask import Flask, session, redirect, url_for, escape, request, render_template

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'


@app.route('/', methods=['GET', 'POST'])
def index():
    if 'username' not in session:
        return redirect(url_for('login'))
    elif request.method == 'GET':
        return render_template('test_render.html', name=session['username'])
    elif request.method == 'POST':
        return redirect(url_for('testwithparam', parameter=request.form['param']))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET' and 'username' in session:
        return redirect(url_for('index'))
    elif request.method == 'GET' and 'username' not in session:
        return render_template('login_page.html')
    elif request.method == 'POST':
        session['username'] = request.form['username']
        return redirect(url_for('index'))


@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('index'))


@app.route('/testwithparam/<parameter>')
def testwithparam(parameter):
    return render_template('test_with_params.html', parameter=parameter)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404
